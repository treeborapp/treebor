from config.settings.base import *

ALLOWED_HOSTS = ["localhost", "treebor.org", "www.treebor.org"]
APP_URL = "https://www.treebor.org"
DEPLOY_PORT=os.environ.get("DEPLOY_PORT", "31415")

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.environ.get("POSTGRES_NAME"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": "db",
        "PORT": os.environ.get("POSTGRES_PORT"),
    }
}
