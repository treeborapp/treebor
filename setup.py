from setuptools import setup, find_packages

NAME = "treebor"
VERSION = "0.0.1"

setup(name=NAME,
      version=VERSION,
      description="Ever wanted trees for neighbors?",
      author="Benny Mei",
      author_email="me@bennymei.com",
      url="https://www.treebor.org/",
      packages=find_packages())
