# syntax=docker/dockerfile:1
FROM python:3.9-buster
RUN apt-get update

# needed to properly run PostGIS
RUN apt-get install -y binutils libproj-dev gdal-bin
# needed to properly run spatialite
RUN apt-get install -y libsqlite3-mod-spatialite
RUN apt-get install -y postgresql-client

ENV PYTHONUNBUFFERED=1
ENV DJANGO_SETTINGS_MODULE=config.settings.local
ENV POSTGRES_NAME=
ENV POSTGRES_USER=
ENV POSTGRES_PASSWORD=
ENV POSTGRES_PORT=
ENV POSTGRES_HOST=
ENV DEPLOY_PORT=
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
