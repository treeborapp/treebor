# Treebor

## Prerequisites
Treebor is a Django app using Postgres for a database with the PostGIS
extension. Treebor is tested with Python 3.9.0, but other versions of
Python 3 are likely to work just as well.

If this is your first time setting up a Python project, we reocmmend
using `pyenv` to setup your virtual environment. You can read more about
pyenv on their [page][1].

Treebor uses Postgres 12 with the PostGIS extension. If you use Ubuntu, you can
install postges via apt, e.g.

```
apt install postgresql-12 postgresql-client-12
apt install postgis postgresql-12-postgis-3
apt install postgresql-12-postgis-3-scripts
```

Once Postgres is installed, make sure to enable to PostGIS extension

```
$ psql -d my_db
mydb=# CREATE EXTENSION postgis;
```

## Setup
```
pip install -r requirements.txt
```

## Run
```
python manage.py runserver 35432
```

```
gunicorn -c config/gunicorn.py wsgi:application
```
```
docker-compose build
docker-compose up
```
```
docker-compose -f docker-compose-beta.yml build
docker-compose -f docker-compose-beta.yml up
```
```
docker build . -t treebor
docker run -it treebor bash
```


[1]: https://github.com/pyenv/pyenv
