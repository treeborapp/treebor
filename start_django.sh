#!/bin/bash

RETRIES=7
while [ "$RETRIES" -gt 0 ]
do
    echo "Waiting for postgres server $((RETRIES--)) remaining attempts"
    PG_STATUS="$(pg_isready -h db -U postgres)"
    PG_EXIT=$(echo $?)
    echo "Postgres Status: $PG_EXIT - $PG_STATUS"
    if [ "$PG_EXIT" = "0" ];
        then
            RETRIES=0
        fi
    sleep 5
done

python manage.py migrate
gunicorn -c config/gunicorn.py treebor.wsgi:application
