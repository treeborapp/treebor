"""treebor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from apps.trees.views import TreesList
from apps.core import views as core_views

urlpatterns = [
    path('api/v1/trees', TreesList.as_view()),
    path('admin/', admin.site.urls),
    path('explore', core_views.explore, name="explore_mode"),
    path('privacy_policy', core_views.privacy_policy, name="privacy_policy"),
    path('about_us', core_views.about_us, name="about_us"),
    path('', core_views.index),
]
