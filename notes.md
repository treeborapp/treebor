# Install PostGIS
https://postgis.net/install/
https://wiki.ubuntu.com/UbuntuGIS
https://trac.osgeo.org/ubuntugis/wiki/QuickStartGuide
https://trac.osgeo.org/ubuntugis/wiki/UbuntuGISRepository
https://launchpad.net/~ubuntugis/+archive/ubuntu/ppa/+packages?field.name_filter=post&field.status_filter=published&field.series_filter=

# Updating Postgres Container with New Data
Run the docker-compose file for the correct environment and let the database come up.
Then you'll need to start another docker container so you can run django commands.
When starting this docker container, pass environment variables that define how to connect
to the postgres database. The django app will need this information to actually connect.
Additionally, you will need to specify the network that the database is on.

For example
```
docker run -it --rm --network=treebor_local_net -e POSTGRES_NAME='postgres' -e POSTGRES_USER='postgres' -e POSTGRES_PASSWORD='postgres' -e 'POSTGRES_PORT'=5432 treebor_local_test /bin/bash
```

Alternatively, you can define the environment variables inside the docker container so you
don't need to pass them at runtime.

Spatialite does support Point distance calculations per https://docs.djangoproject.com/en/4.0/ref/contrib/gis/model-api/#selecting-an-srid-1 so we need to use postgis as our database.

You can run two docker containers and have them communicate with each other. See https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach

# Gunicorn is exposed
How do we stop gunicorn from being directly exposed to the internet?

Right now, we can go to http://beta.treebor.org:27182/ and it will resolve.
