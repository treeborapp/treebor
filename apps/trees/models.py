from django.contrib.gis.db import models
from apps.core.models import TimeStampedModel, SoftDeleteModel


class TreeDataSource(models.Model):
    data_source = models.CharField(max_length=255)


class TreeScientificName(models.Model):
    scientific_name = models.TextField()


class TreeCommonName(models.Model):
    common_name = models.TextField()


class Tree(TimeStampedModel, SoftDeleteModel):
    id = models.BigAutoField(primary_key=True)
    data_source = models.ForeignKey("TreeDataSource", on_delete=models.CASCADE)
    tree_id = models.CharField(max_length=255)
    scientific_name = models.ForeignKey("TreeScientificName", on_delete=models.CASCADE)
    common_name = models.ForeignKey("TreeCommonName", on_delete=models.CASCADE)
    latlong = models.PointField()

    class Meta:
        unique_together = [["tree_id", "data_source"]]
