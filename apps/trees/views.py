from django.shortcuts import render

from django.contrib.gis.geos import Point

from rest_framework import status

from rest_framework.views import APIView
from rest_framework.response import Response
from apps.trees.serializers import NearbyTreeSerializer, TreeSerializer
from apps.trees.models import Tree
from apps.core.services import get_client_ip
from apps.telemetry.models import UserAccess, UserAction


# Create your views here.
class TreesList(APIView):
    def get(self, request, format=None):
        return Response("Hello World")

    def post(self, request, format=None):
        serializer = NearbyTreeSerializer(data=request.data)

        user_ip_address = get_client_ip(request)
        UserAccess.objects.create(
            ip_address=user_ip_address,
            action=UserAction.objects.get_or_create(action="get_trees")[0],
        )

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        latitude = serializer.validated_data["latitude"]
        longitude = serializer.validated_data["longitude"]
        location = Point(longitude, latitude)
        trees = Tree.objects.filter(latlong__distance_lt=(location, 120))
        tree_serializer = TreeSerializer(trees, many=True)
        return Response(tree_serializer.data)
