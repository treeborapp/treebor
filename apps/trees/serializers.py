from rest_framework import serializers
from apps.trees.models import Tree, TreeScientificName, TreeCommonName


class NearbyTreeSerializer(serializers.Serializer):
    latitude = serializers.FloatField()
    longitude = serializers.FloatField()


class ScientificNameSerializer(serializers.Serializer):
    class Meta:
        model = TreeScientificName
        fields = ["scientific_name"]


class CommonNameSerializer(serializers.Serializer):
    class Meta:
        model = TreeCommonName
        fields = ["common_name"]


class TreeSerializer(serializers.Serializer):
    scientific_name = ScientificNameSerializer()
    common_name = CommonNameSerializer()

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret.pop("latlong", None)
        ret["latitude"] = f"{instance.latlong.y}"
        ret["longitude"] = f"{instance.latlong.x}"
        ret["scientific_name"] = instance.scientific_name.scientific_name
        ret["common_name"] = instance.common_name.common_name
        return ret

    class Meta:
        model = Tree
        fields = ["tree_id", "latlong", "scientific_name", "common_name"]
