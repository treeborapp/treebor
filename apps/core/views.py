from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from django.conf import settings

# from apps.core.services import get_client_ip
# from apps.telemetry.models import UserAction, UserAccess

# Create your views here.
def index(request):
    template = loader.get_template("core/index.html")
    context = {"app_url": settings.APP_URL}

    # user_ip_address = get_client_ip(request)
    # UserAccess.objects.create(
    #     ip_address = user_ip_address,
    #     action = UserAction.objects.get_or_create(action="nearby_mode")[0]
    # )

    return HttpResponse(template.render(context, request))


def explore(request):
    template = loader.get_template("core/explore.html")
    context = {"app_url": settings.APP_URL}
    # user_ip_address = get_client_ip(request)
    # UserAccess.objects.create(
    #     ip_address = user_ip_address,
    #     action = UserAction.objects.get_or_create(action="explore_mode")[0]
    # )
    return HttpResponse(template.render(context, request))


def privacy_policy(request):
    template = loader.get_template("core/privacy_policy.html")
    context = {"app_url": settings.APP_URL}
    return HttpResponse(template.render(context, request))


def about_us(request):
    template = loader.get_template("core/about_us.html")
    context = {"app_url": settings.APP_URL}
    return HttpResponse(template.render(context, request))
