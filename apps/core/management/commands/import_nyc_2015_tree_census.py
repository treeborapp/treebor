from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from apps.trees.models import TreeDataSource, TreeScientificName, TreeCommonName, Tree
import csv


class Command(BaseCommand):
    help = "import tree data from the NYC 2015 tree census"

    def add_arguments(self, parser):
        parser.add_argument("filename", nargs=1, type=str)

    def handle(self, *args, **options):

        DATASOURCE_NAME = "nyc_2015_tree_census"

        self.stdout.write(f"Processing {options['filename'][0]}")
        filename = options["filename"]
        datasource_name = DATASOURCE_NAME
        self.stdout.write(f"Datasource {datasource_name}")
        tree_data_source, created = TreeDataSource.objects.get_or_create(
            data_source=datasource_name
        )
        self.stdout.write(f"Created new tree data source: {created}")

        csvfile = open(filename[0], newline="")
        reader = csv.DictReader(csvfile)
        trees_saved = 0
        rows_processed = 0
        for row in reader:
            rows_processed += 1
            if row["spc_latin"] == "":
                continue
            scientific_name, _ = TreeScientificName.objects.get_or_create(
                scientific_name=row["spc_latin"]
            )
            common_name, _ = TreeCommonName.objects.get_or_create(
                common_name=row["spc_common"]
            )
            location = Point(float(row["longitude"]), float(row["latitude"]))
            tree = Tree.objects.update_or_create(
                data_source=tree_data_source,
                tree_id=row["tree_id"],
                scientific_name=scientific_name,
                common_name=common_name,
                latlong=location,
            )
            trees_saved += 1
            if trees_saved % 500 == 0:
                self.stdout.write(f"Saved {trees_saved} trees")
        csvfile.close()
        message = f"Processed {rows_processed} rows and saved {trees_saved} trees"
        return message
