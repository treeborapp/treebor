def get_client_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
    if x_forwarded_for:
        ip_addresses = x_forwarded_for.split(",")
        ip = ip_addresses[-1].strip()
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip
