from django.db import models

from apps.core.models import TimeStampedModel


class UserAction(models.Model):
    action = models.CharField(max_length=255)


class UserAccess(models.Model):
    visit_time = models.DateTimeField(auto_now_add=True)
    ip_address = models.GenericIPAddressField()
    action = models.ForeignKey("UserAction", on_delete=models.CASCADE)
